<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>REGEMAR</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

   <!-- Bootstrap 3.3.7 -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body style="margin:0px">
    <div class = "welcomebackground">
    <div class="container">
    <div class="RegisterTitle">
                    Registro de gente de mar
    </div>
    </div>
<div class="container2">
    <div class="login">
        <form method="post" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            <div class="inputcenterlogin">
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            
                <input type="email" class="inputtextlogin" name="email" value="{{ old('email') }}" placeholder="Correo Electronico">
            
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="inputtextlogin" placeholder="Contraseña" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="buttonlogin">Ingresar</button>
                </div>
                <!-- /.col -->
            </div>
            </div>
        </form>
</div></div>
    <!-- mini menu
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Ingresar</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

           
        </div>-->
        </div>
    </body>
</html>


