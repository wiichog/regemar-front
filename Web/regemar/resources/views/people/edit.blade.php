<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>REGEMAR</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    </head>
    <body style="margin:0px">
    	{{ csrf_field() }}
<h4>Edicion de datos de: {{$people[0]->name}} {{$people[0]->lastname}}</h4>
<form method="GET" role="form" action="{{ route('/people/update') }}">
	<div class="divNewPerson">
		<div class="divTable">
			<div class="divTableBody">
				<div class="divTableRow">
					<div class="divTableCell">Nombres: </div>
					<div class="divTableCell"><input type="text" id="name" name="name" placeholder="Ingrese Nombre" value="{{$people[0]->name}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Apellidos: </div>
					<div class="divTableCell"><input type="text" name="lastname" placeholder="Ingrese Apellidos" value="{{$people[0]->lastname}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Fecha de Nacimiento: </div>
					<div class="divTableCell"><input type="date" data-date-inline-picker="true" name="birthday" value="{{$people[0]->birthday}}"/></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Nacionalidad: </div>
					<div class="divTableCell"><input type="text" name="nationality" placeholder="Ingrese Nacionalidad" value="{{$people[0]->nacionality}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Pais de Origen: </div>
					<div class="divTableCell"><input type="text" name="country" placeholder="Ingrese Pais de Origen" value="{{$people[0]->country}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Departamento: </div>
					<div class="divTableCell"><input type="text" name="deparment" placeholder="Ingrese Departamento" value="{{$people[0]->deparment}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">DPI: </div>
					<div class="divTableCell"><input type="text" name="dpi" placeholder="Ingrese DPI" value="{{$people[0]->DPI}}" disabled></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Email: </div>
					<div class="divTableCell"> <input type="text" name="email" placeholder="Ingrese email" value="{{$people[0]->email}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Direccion: </div>
					<div class="divTableCell"><input type="text" name="address" placeholder="Ingrese direccion" value="{{$people[0]->address}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Telefono de Casa: </div>
					<div class="divTableCell"><input type="text" name="homephone" placeholder="Ingrese Telefono de Casa" value="{{$people[0]->homephone}}" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Telefono Personal: </div>
					<div class="divTableCell"><input type="text" name="personalphone" placeholder="Ingrese Telefono Personal" value="{{$people[0]->personalphone}}" required></div>
				</div>

				<div class="divTableRow">
					<div class="divTableCell">Sexo: </div>
					<div class="divTableCell">
					@if($people[0]->gender == 'male') 
					<input type="radio" name="gender" value="male" checked >Masculino<br>
					<input type="radio" name="gender" value="female">Femenino<br>
					@else
					<input type="radio" name="gender" value="male">Masculino<br>
					<input type="radio" name="gender" value="female" checked>Femenino<br>
					@endif
				</div>
					
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Estatura: </div>
					<div class="divTableCell"><input type="text" name="height" placeholder="Ingrese Estatura" value="{{$people[0]->height}}"></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Peso: </div>
					<div class="divTableCell"><input type="text" name="weight" placeholder="Ingrese Peso" value="{{$people[0]->weight}}"></div>
				</div>
			</div>
		</div>
	</div>
	
				<div class="divPicturePerson">
					<div class="personPicture" id="personPicture"></div>

						<div class="fileUpload btn btn-primary">
						    <span>Cambiar foto</span>
						    <input type="file" name="uploadPicture" id="uploadPicture" class="upload" />
						</div><p>
						    <button type="submit" value="Submit" class="btn btn-success">Editar Persona</button>
					</div>					
					
				</form>
				
    </body>
</html>




