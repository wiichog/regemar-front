<div class="row">
	<div class="col-sm-6">
		<h2>Busqueda de Personas</h2>
	</div>
</div>
<div class="form-group">
  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busqueda por Nombre" class="form-control">
</div>

<div class="panel mini-box">
	
	<table class="table table-stripped" id="mytable">
		<thead>
			<tr>
				<th>Nombre</th>
				<th colspan="2">DPI</th>
			</tr>
		</thead>
			@foreach($peoples as $people)
				<tr>
					<td>{{$people->person_name}}</td>
					<td>{{$people->person_dpi}}</td>
					<td>
						<a class="btn btn-info" onclick="people_edit({{$people->person_dpi}})"><i class="fa fa-pencil"></i></a>&nbsp;
						<form style="display:inline;" method="POST" action="/people">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE">
							<input type="hidden" name="id" value="{{ $people->person_dpi }}">
							<input type="submit" class="btn btn-danger" value="Eliminar">
							

						</form>
					</td>
				</tr>
			@endforeach
	</table>
</div>

<script type="text/javascript">
            function people_edit(dpi)
            {
                $('#content').load('/people/edit/' + dpi + '');
            }
</script>
<script>
function myFunction() {
  // Declare variables 

  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("mytable");
  console.log(table)
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>
