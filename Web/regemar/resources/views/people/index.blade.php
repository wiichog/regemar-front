<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>REGEMAR</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    </head>
    <body style="margin:0px">
    	{{ csrf_field() }}
    	<script type="text/javascript"> 
					
					document.getElementById('uploadPicture').addEventListener('change', readURL, true);
function readURL(){
					   var file = document.getElementById("uploadPicture").files[0];
					   var reader = new FileReader();
					   reader.onloadend = function(){
					      document.getElementById('personPicture').style.backgroundImage = "url(" + reader.result + ")";        
					   }
					   if(file){
					      reader.readAsDataURL(file);
					    }else{
					    }
					}
				</script>
<h4>Ingreso de Datos</h4>
<form method="GET" role="form" action="{{ route('person/create') }}">
	<div class="divNewPerson">
		<div class="divTable">
			<div class="divTableBody">
				<div class="divTableRow">
					<div class="divTableCell">Nombres: </div>
					<div class="divTableCell"><input type="text" id="name" name="name" placeholder="Ingrese Nombre" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Apellidos: </div>
					<div class="divTableCell"><input type="text" name="lastname" placeholder="Ingrese Apellidos" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Fecha de Nacimiento: </div>
					<div class="divTableCell"><input type="date" data-date-inline-picker="true" name="birthday" required /></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Nacionalidad: </div>
					<div class="divTableCell"><input type="text" id="nationality" name="nationality" placeholder="Ingrese Nacionalidad" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Pais de Origen: </div>
					<div class="divTableCell"><input type="text" name="country" placeholder="Ingrese Pais de Origen" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Departamento: </div>
					<div class="divTableCell"><input type="text" name="deparment" placeholder="Ingrese Departamento" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">DPI: </div>
					<div class="divTableCell"><input type="text" name="dpi" placeholder="Ingrese DPI" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Email: </div>
					<div class="divTableCell"> <input type="text" name="email" placeholder="Ingrese email" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Direccion: </div>
					<div class="divTableCell"><input type="text" name="address" placeholder="Ingrese direccion" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Telefono de Casa: </div>
					<div class="divTableCell"><input type="text" name="homephone" placeholder="Ingrese Telefono de Casa" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Telefono Personal: </div>
					<div class="divTableCell"><input type="text" name="personalphone" placeholder="Ingrese Telefono Personal" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Sexo: </div>
					<div class="divTableCell"><input type="radio" name="gender" value="male">Masculino<br>
					<input type="radio" name="gender" value="female">Femenino<br></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Estatura: </div>
					<div class="divTableCell"><input type="text" name="height" placeholder="Ingrese Estatura" required></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell">Peso: </div>
					<div class="divTableCell"><input type="text" name="weight" placeholder="Ingrese Peso" required></div>
				</div>
			</div>
		</div>
	</div>
	
				<div class="divPicturePerson">
					<div class="personPicture" id="personPicture"></div>

						<div class="fileUpload btn btn-primary">
						    <span>Subir foto</span>
						    <input type="file" name="uploadPicture" id="uploadPicture" class="upload" />
						</div><p>
						<div class="fileUpload btn btn-primary">
						    <span>Subir DPI</span>
						    <input type="file" name="uploadDPI" id="uploadDPI" class="upload" />
						</div><p>
						<div class="fileUpload btn btn-primary">
						    <span>Subir Certificado Medico</span>
						    <input type="file" name="uploadMeds" id="uploadMeds" class="upload" />
						</div><p>
						<div class="fileUpload btn btn-primary">
						    <span>Subir diploma de escuela</span>
						    <input type="file" name="uploadSchool" id="uploadSchool" class="upload" />
						</div><p>
						<div class="fileUpload btn btn-primary">
						    <span>Subir solicitud de certificado</span>
						    <input type="file" name="uploadCertificate" id="uploadCertificate" class="upload" />
						</div><p>
						<button type="submit" value="Submit" class="btn btn-success">Crear Persona</button>
					</div>					
				
				</form>
				
    </body>
</html>




