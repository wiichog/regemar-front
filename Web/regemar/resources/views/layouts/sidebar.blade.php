<aside class="main-sidebar" id="sidebar-wrapper">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                @if (Auth::guest())
                <p>InfyOm</p>
                @else
                    <p> Bienvenido {{ Auth::user()->name}}</p>
                @endif
                <!-- Status -->
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <ul id="nav" class="nav" style="overflow: hidden; width: auto; height: 100%;">
            <li class="{{ Request::path() == '/' ? 'active' : '' }}">
                <a href="/home">
                    <i class="fa fa-home">
                        <span class="icon-bg bg-danger"></span>
                    </i>
                    <span class="ng-scope">Inicio</span> 
                </a>
            </li>
            <li onclick="people_content()" class="pointer">
                <a>
                    <i class="fa fa-pencil-square-o">
                        <span class="icon-bg bg-warning"></span>
                    </i>
                    <span class="ng-scope">Nueva Persona</span> 
                </a>
            </li>
            <li onclick="people_edit()" class="pointer">
                <a>
                    <i class="fa fa-search">
                        <span class="icon-bg bg-primary"></span>
                    </i>
                    <span class="ng-scope">Edicion de Persona</span> 
                </a>
            </li>


            </ul>
        </form>
        <!-- Sidebar Menu -->
        <script type="text/javascript">
            function people_content()
            {
                $('#content').load('/people/');
            }
            function people_edit()
            {
                $('#content').load('/people/editmenu/');
            }
        </script>
        <ul class="sidebar-menu">
            @include('layouts.menu')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>