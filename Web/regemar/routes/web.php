<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/people', 'PeopleController@index');
Route::delete('/people', 'PeopleController@delete');
Route::get('/people/editmenu', 'PeopleController@editmenu');
Route::get('/people/edit/{dpi}','PeopleController@edit');
Route::get('/people/update', 'PeopleController@update')->name('/people/update');
Route::get('/ftp', ['as ' => 'ftp', 'uses' => 'FtpController@index']);
//Route::resource('person/create', 'PeopleController@create');
Route::get('person/create', 'PeopleController@create')->name('person/create');
