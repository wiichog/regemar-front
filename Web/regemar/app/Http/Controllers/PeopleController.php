<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Input;


class PeopleController extends Controller
{
    public function index() {
		return view('people.index');
	}

	public function create(Request $request){
		\FTP::connection()->makeDir("persons/". $request->dpi ."");
		DB::table('people')->insert(
		 [
		    'name' => $request->name,
		    'lastname' => $request->lastname,
		    'birthday' => $request->birthday,
		    'nacionality' => $request->nationality,
		    'country' => $request->country,
		    'deparment' => $request->deparment,
		    'DPI' => $request->dpi,
		    'email' => $request->email,
		    'gender' => $request->gender,
		    'address' => $request->address,
		    'homephone' => $request->homephone,
		    'personalphone' => $request->personalphone,
		    'height' => $request->height,
		    'weight' => $request->weight
		 ]
		);
		return view('home.index')->with;
	}

	public function editmenu(){
		return view('people.editmenu')->with("peoples", DB::table('people')->select('name as person_name', 'DPI as person_dpi')->get());
	}

	public function edit($dpi){
		return view('people.edit')->with("people", DB::table('people')->where('DPI', $dpi)->get());
	}

	public function delete(Request $request) {
		$person = DB::table('people')->where('DPI', $request->id);
		$person->delete();
		return redirect("/people");
	}

	public function update(Request $request){
		DB::table('people')
            ->where('dpi', $request->dpi)
            ->update([
            'name' => $request->name,
		    'lastname' => $request->lastname,
		    'birthday' => $request->birthday,
		    'nacionality' => $request->nationality,
		    'country' => $request->country,
		    'deparment' => $request->deparment,
		    'DPI' => $request->dpi,
		    'email' => $request->email,
		    'gender' => $request->gender,
		    'address' => $request->address,
		    'homephone' => $request->homephone,
		    'personalphone' => $request->personalphone,
		    'height' => $request->height,
		    'weight' => $request->weight
            ]);
    return view('people.edit')->with("people", DB::table('people')->where('DPI', $request->dpi)->get());
	}
}
