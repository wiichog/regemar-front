﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;
//using iTextSharp.text.pdf;
//using iTextSharp.text;

//namespace RegistroGentedeMar
//{
//    class generateDocument
//    {
//        public void course(String correlative,String pictureLocation, String namePerson, String lastnamePerson, String courseNameSpanish, String courseNameEnglish, String dateIssued, String dateExpiration)
//        {
//            //Generar y Abrir documento y writer
//            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
//            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("GUA-DGAM-CERT-" + correlative + "-2017.pdf", FileMode.Create));
//            doc.Open();
//            PdfContentByte cb = wri.DirectContent;

//            //Agregar imagen de persona y escudo de gt
//            //iTextSharp.text.Image foto = iTextSharp.text.Image.GetInstance(pictureLocation PersonPicture.ImageLocation);
//            //foto.SetAbsolutePosition(43f, doc.PageSize.Height - 155f);
//            //foto.ScaleToFit(100f, 1000f);
//            //doc.Add(foto);

//            iTextSharp.text.Image escudo = iTextSharp.text.Image.GetInstance("gt.png");
//            escudo.SetAbsolutePosition(240f, doc.PageSize.Height - 100f);
//            escudo.ScaleToFit(60f, 90f);
//            doc.Add(escudo);

//            //Fuentes a utilizar
//            var arial9 = FontFactory.GetFont("Arial", 9f, BaseColor.BLACK);
//            var arial10 = FontFactory.GetFont("Arial", 10f, BaseColor.BLACK);
//            var arial11 = FontFactory.GetFont("Arial", 11f, BaseColor.BLACK);
//            var arial12 = FontFactory.GetFont("Arial", 12f, BaseColor.BLACK);
//            var arial14 = FontFactory.GetFont("Arial", 14f, BaseColor.BLACK);

//            //Texto de los campos de las personas
//            string nombrePersona = namePerson + " " + lastnamePerson;
//            string nombreCursoEspanol = courseNameSpanish;
//            string nombreCursoIngles = courseNameEnglish;
//            //string fechaExpedicion = dateIssued      monthCalendar2.SelectionStart.ToShortDateString();
//            //string fechaExpiracion = dateExpiration monthCalendar3.SelectionStart.ToShortDateString();

//            //Generar y agregar Texto
//            Chunk c1 = new Chunk("REPÚBLICA DE GUATEMALA")
//            {
//                Font = arial12
//            };
//            Chunk c2 = new Chunk("\nREPUBLIC OF GUATEMALA")
//            {
//                Font = arial9
//            };
//            Chunk c3 = new Chunk("\nDIRECCIÓN GENERAL DE ASUNTOS MARÍTIMOS")
//            {
//                Font = arial12
//            };
//            Chunk c4 = new Chunk("\nGENERAL MARITIME AFFAIRS DIVISION")
//            {
//                Font = arial9
//            };
//            Chunk c5 = new Chunk("\n\n\nLA DIRECCIÓN GENERAL DE ASUNTOS MARÍTIMOS, EN USO DE LAS FACULTADES QUE EL GOBIERNO DE LA REPÚBLICA LE CONFIERE EN EL ACUERDO GUBERNATIVO 65 - 2014, OTORGA EL PRESENTE CERTIFICADO A: ")
//            {
//                Font = arial12
//            };
//            Chunk c6 = new Chunk("\nTHE GENERAL MARITIME AFFAIRS DIVISION, WITH THE FACULTY CONFERED BY THE GOVERNMENT ACCORDING TO THE GUBERNATIVE AGREEMENT 65 - 2014 ISSUES THE PRESENT CERTIFICATE TO:\n\n")
//            {
//                Font = arial9
//            };
//            Chunk c7 = new Chunk(nombrePersona)
//            {
//                Font = arial12
//            };
//            Chunk c8 = new Chunk("\n\nPOR HABER APROBADO EL CURSO DE")
//            {
//                Font = arial12
//            };
//            Chunk c9 = new Chunk("\nHAS APROVED\n\n")
//            {
//                Font = arial9
//            };
//            Chunk c10 = new Chunk(nombreCursoEspanol + "\n")
//            {
//                Font = arial12
//            };
//            Chunk c11 = new Chunk(nombreCursoIngles)
//            {
//                Font = arial9
//            };
//            Chunk c12 = new Chunk("\n\nDE ACUERDO A LAS REGULACIONES RELATIVAS AL CONVENIO INTERNACIONAL SOBRE NORMAS DE FORMACIÓN, TITULACIÓN Y GUARDIA PARA LA GENTE DE MAR, 1978, Y SUS ENMIENDAS, REGLA VI/ 1.")
//            {
//                Font = arial12
//            };
//            Chunk c13 = new Chunk("\nACCORDING TO REGULATIONS RELATIVE TO INTERNATIONAL CONVENTION STANDARDS OF")
//            {
//                Font = arial9
//            };
//            Chunk c14 = new Chunk("\nTRAINING, CERTIFICATION AND WATCHKEEPING FOR SEAFARERS, 1978, AND AMENDMENTS")
//            {
//                Font = arial9
//            };
//            Chunk c15 = new Chunk("\nREGULATION VI/ 1.\n\n")
//            {
//                Font = arial9
//            };
//            Chunk c16 = new Chunk("\nFECHA DE  EXPEDICIÓN / DATE OF ISSUE : " + fechaExpedicion +
//                                  "\nFECHA DE EXPIRACIÓN / DATE OF EXPIRY : " + fechaExpiracion)
//            {
//                Font = arial12
//            };
//            Chunk c17 = new Chunk("\n\n\n\n" +
//                                 "DIRECCIÓN GENERAL                    DIRECCIÓN DE TITULACIÓN" +
//                               "\nASUNTOS MARÍTIMOS                               Y GENTE DE MAR" +
//                               "\n\n\n______________________                   ______________________")
//            {
//                Font = arial11
//            };

//            Chunk c18 = new Chunk("No.")
//            {
//                Font = arial9
//            };
//            Chunk c19 = new Chunk("GUA-DGAM-CERT-" + CourseCorrelative.Text + "-2017\n")
//            {
//                Font = arial14
//            };
//            Chunk c20 = new Chunk("CERTIFICADO\nCERTIFICATE")
//            {
//                Font = arial10
//            };

//            //Columna2 en el documento
//            ColumnText column1 = new ColumnText(cb);
//            ColumnText column2 = new ColumnText(cb);

//            //column1.SetSimpleColumn(35, 30, 35 * 10, 30 * 18);
//            column1.SetSimpleColumn(60, 35, doc.PageSize.Width - 60f, doc.PageSize.Height - 100f);
//            column2.SetSimpleColumn(200f, doc.PageSize.Height - 40f, doc.PageSize.Width - 80f, doc.PageSize.Height - 120f);
//            Paragraph p = new Paragraph()
//            {
//                Alignment = Element.ALIGN_CENTER
//            };
//            Paragraph p1 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_RIGHT
//            };
//            p.Add(c1);
//            p.Add(c2);
//            p.Add(c3);
//            p.Add(c4);
//            p.Add(c5);
//            p.Add(c6);
//            p.Add(c7);
//            p.Add(c8);
//            p.Add(c9);
//            p.Add(c10);
//            p.Add(c11);
//            p.Add(c12);
//            p.Add(c13);
//            p.Add(c14);
//            p.Add(c15);
//            p.Add(c16);
//            p.Add(c17);
//            column1.AddElement(p);
//            column1.Go();
//            p1.Add(c18);
//            p1.Add(c19);
//            p1.Add(c20);
//            column2.AddElement(p1);
//            column2.Go();
//            doc.Close();
//        }
//        public void medical()
//        {
//            //Generar y Abrir documento y writer
//            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
//            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("GUA-DGAM-CERT-" + CorrelativeMedics.Text + "-2017.pdf", FileMode.Create));
//            doc.Open();
//            PdfContentByte cb = wri.DirectContent;

//            //Agregar imagen de persona y escudo de gt
//            iTextSharp.text.Image foto = iTextSharp.text.Image.GetInstance(PersonPicture.ImageLocation);
//            foto.SetAbsolutePosition(43f, doc.PageSize.Height - 155f);
//            foto.ScaleToFit(100f, 1000f);
//            doc.Add(foto);

//            iTextSharp.text.Image escudo = iTextSharp.text.Image.GetInstance("gt.png");
//            escudo.SetAbsolutePosition(240f, doc.PageSize.Height - 100f);
//            escudo.ScaleToFit(60f, 90f);
//            doc.Add(escudo);

//            //Fuentes a utilizar
//            var arial8 = FontFactory.GetFont("Arial", 8f, BaseColor.BLACK);
//            var arial9 = FontFactory.GetFont("Arial", 9f, BaseColor.BLACK);
//            var arial10 = FontFactory.GetFont("Arial", 10f, BaseColor.BLACK);
//            var arial11 = FontFactory.GetFont("Arial", 11f, BaseColor.BLACK);
//            var arial12 = FontFactory.GetFont("Arial", 12f, BaseColor.BLACK);
//            var arial14 = FontFactory.GetFont("Arial", 14f, BaseColor.BLACK);

//            //Texto de los campos de las personas
//            string nombrePersona = PersonName.Text;
//            string apellidoPersona = PersonLastName.Text;
//            string fechaNacimiento = Birthday.Text;
//            string lugarNacimiento = BirthPlace.Text;


//            //Generar y agregar Texto
//            Chunk c1 = new Chunk("REPÚBLICA DE GUATEMALA")
//            {
//                Font = arial10
//            };
//            Chunk c2 = new Chunk("\nREPUBLIC OF GUATEMALA")
//            {
//                Font = arial9
//            };
//            Chunk c3 = new Chunk("\nDIRECCIÓN GENERAL DE ASUNTOS MARÍTIMOS")
//            {
//                Font = arial12
//            };
//            Chunk c4 = new Chunk("\nGENERAL MARITIME AFFAIRS DIVISION")
//            {
//                Font = arial9
//            };
//            Chunk c5 = new Chunk("\n\nLA DIRECCIÓN GENERAL DE ASUNTOS MARÍTIMOS, EN USO DE LAS FACULTADES QUE EL GOBIERNO DE LA REPÚBLICA LE CONFIERE EN EL ACUERDO GUBERNATIVO 65 - 2014, OTORGA EL PRESENTE CERTIFICADO MEDICO DE APTITUD PARA EMBARQUE A: ")
//            {
//                Font = arial10
//            };
//            Chunk c6 = new Chunk("\nTHE GENERAL MARITIME AFFAIRS DIVISION, WITH THE FACULTY CONFERED BY THE GOVERNMENT ACCORDING TO THE GUBERNATIVE AGREEMENT 65 - 2014 ISSUES THE PRESENT CERTIFICATE TO:\n\n")
//            {
//                Font = arial9
//            };
//            Chunk c7 = new Chunk("                                                                   FINESS FOR SHIPMENT TO:")
//            {
//                Font = arial9
//            };

//            Chunk c8 = new Chunk("\nDeclaración del facultativo reconocido /  ")
//            {
//                Font = arial12
//            };

//            Chunk c9 = new Chunk("Declaration of the recognized medical practitioner")
//            {
//                Font = arial9
//            };

//            Chunk c10 = new Chunk("\n1.	Confirmación de que se examinaron los documentos de identidad en el punto de examen: SI / NO")
//            {
//                Font = arial11
//            };

//            Chunk c11 = new Chunk("\n1.	 Confirmation that identification documents were checked at the point of examination: Y/N")
//            {
//                Font = arial9
//            };

//            Chunk c12 = new Chunk("\n\n2.La audición satisface las normas de la sección A - I / 9 del Código de Formación: SI / NO")
//            {
//                Font = arial11
//            };

//            Chunk c13 = new Chunk("\n2.Hearing meets the standards in STCW A-I / 9: Y / N")
//            {
//                Font = arial9
//            };

//            Chunk c14 = new Chunk("\n\n3.	¿Es satisfactoria la audición sin audífonos?: SI / NO")
//            {
//                Font = arial11
//            };

//            Chunk c15 = new Chunk("\n3.	Unaided hearing satisfactory? Y/N")
//            {
//                Font = arial9
//            };

//            Chunk c16 = new Chunk("\n\n4.	¿La agudeza visual cumple las normas de la sección A-I/9 del Código de Formación?:SI / NO    (Aplica sólo a Marineros que formen parte de la guardia de navegación)")
//            {
//                Font = arial11
//            };

//            Chunk c17 = new Chunk("\n4.	 Visual acuity meets standards in STCW A-I/9? Y/N      (Applies only to Mariners that are part of the guard navigation)")
//            {
//                Font = arial9
//            };

//            Chunk c21 = new Chunk("\n\n5.	¿Cumple la visión cromática las normas de la sección A-I-9 del Código de Formación?:SI/NO      (Aplica sólo a Marineros que formen parte de la guardia de navegación)")
//            {
//                Font = arial11
//            };

//            Chunk c22 = new Chunk("\n5.	 Colour vision meets standards in STCW A-I/9? Y/N          (Applies only to Mariners that are part of the guard navigation)")
//            {
//                Font = arial9
//            };

//            Chunk c23 = new Chunk("\n5.1	 Fecha de la última prueba de visión cromática: ")
//            {
//                Font = arial11
//            };

//            Chunk c24 = new Chunk("\n5.1    Date of last colour vision test")
//            {
//                Font = arial9
//            };

//            Chunk c25 = new Chunk("\n\n6.	¿Apto para cometidos de vigía?: SI / NO \n(Aplica sólo a Marineros que formen parte de la guardia de navegación)")
//            {
//                Font = arial11
//            };

//            Chunk c26 = new Chunk("\n6.	 Fit for look-out duties? Y/N\n(Applies only to Mariners that are part of the guard navigation)")
//            {
//                Font = arial9
//            };

//            Chunk c27 = new Chunk("\n7.	¿Existen limitaciones o restricciones respecto de la aptitud física? SI /NO \nSi la Respuesta es “SI” dar detalles de las limitaciones o restricciones")
//            {
//                Font = arial11
//            };

//            Chunk c28 = new Chunk("\n7.	Are there any limitations or restrictions on fitness? YES / NO\nIf the answer is “YES” give details of any limitations or restrictions)")
//            {
//                Font = arial9
//            };

//            Chunk c29 = new Chunk("\n\n8.	¿Está el marino libre de cualquier afección medica que pueda verse agravada por el servicio en el mar o discapacitarle para el desempeño de tal servicio o poner en peligro la salud de otras personas a bordo? SI / NO")
//            {
//                Font = arial11
//            };

//            Chunk c30 = new Chunk("\n8.	Is the seafarer free from any medical condition likely to be aggravated by service at sea or to render the seafarer unfit for such service or to endanger the health of other persons on board?: Y/N")
//            {
//                Font = arial9
//            };

//            Chunk c18 = new Chunk("No.")
//            {
//                Font = arial9
//            };
//            Chunk c19 = new Chunk("GUA-DGAM-CERT-" + CorrelativeMedics.Text + "-2017\n")
//            {
//                Font = arial12
//            };
//            Chunk c20 = new Chunk("CERTIFICADO\nCERTIFICATE")
//            {
//                Font = arial8
//            };

//            Chunk c31 = new Chunk("\n\n\nDE ACUERDO A LAS REGULACIONES RELATIVAS AL CONVENIO INTERNACIONAL SOBRE NORMAS DE FORMACION, TITULACION Y GUARDIA PARA LA GENTE DE MAR, 1978, Y SUS ENMIENDAS, REGLA I/9, CODIGO DE FORMACION, SECCION A-I/9 y B-I/9")
//            {
//                Font = arial12
//            };

//            Chunk c32 = new Chunk("\n\n\nACCORDING TO REGULATIONS RELATIVE TO INTERNATIONAL CONVENTION STANDARDS OF TRAINING, CERTIFICATION AND WATCHKEEPING FOR SEAFARERS, 1978, AND AMENDMENTS. REGULATION I / 9, SECTION A - I / 9 and B - I / 9")
//            {
//                Font = arial9
//            };

//            Chunk c33 = new Chunk("FIRMA DEL MARINO\n\n  ______________________")
//            {
//                Font = arial12
//            };

//            Chunk c34 = new Chunk("\nConfirmo que he sido informado sobre el contenido del presente certificado y del derecho a solicitar una revisión del dictamen con arreglo a lo dispuesto en el párrafo 6 de la sección A-I / 9.")
//            {
//                Font = arial11
//            };

//            Chunk c35 = new Chunk("\nConfirming that the seafarer has been informed of the content of the certificate and of the right to a review in accordance with paragraph 6 of section A-I/9.")
//            {
//                Font = arial9
//            };

//            Paragraph p4 = new Paragraph("\n\nFECHA DEL RECONOCIMIENTO / DATE OF EXAMINATION: xxxxxxxxxxx \nFECHA DE  EXPIRACION / DATE OF EXPIRY: xxxxxxxxxxxx")
//            {
//                Font = arial12,
//                Alignment = Element.ALIGN_LEFT

//            };

//            PdfPTable tabla1 = new PdfPTable(4);
//            Phrase f1 = new Phrase("EXTENDIDO  A  / Issue To \nAPELLIDO / Last Name: " + apellidoPersona + " Name: " + nombrePersona)
//            {
//                Font = arial9
//            };

//            Phrase f2 = new Phrase("Fecha de Nacimiento / Birth Date : " + fechaNacimiento)
//            {
//                Font = arial9
//            };

//            Phrase f3 = new Phrase("Lugar de Nacimiento / Birth Place: " + lugarNacimiento)
//            {
//                Font = arial9
//            };

//            Phrase f4 = new Phrase("Nacionalidad /  Nationality : GUATEMALTECO")
//            {
//                Font = arial9
//            };
//            PdfPCell titulo = new PdfPCell(f1);
//            titulo.Colspan = 4;
//            titulo.HorizontalAlignment = 0;
//            titulo.FixedHeight = 30f;
//            tabla1.AddCell(titulo);
//            tabla1.AddCell(f2);
//            tabla1.AddCell(f3);
//            tabla1.AddCell(f4);

//            if (MaleCheck.Checked == true)
//            {
//                tabla1.AddCell("Sexo / Sex  Hombre / Male");
//            }
//            else if (FemaleCheck.Checked == true)
//            {
//                tabla1.AddCell("Sexo / Sex  Mujer / Female");
//            }
//            else
//            {
//                tabla1.AddCell("Sexo / Sex NA");
//            }
//            tabla1.WidthPercentage = 100;

//            //Columna2 en el documento
//            ColumnText column1 = new ColumnText(cb);
//            ColumnText column2 = new ColumnText(cb);
//            column1.SetSimpleColumn(60f, doc.PageSize.Height - 1000f, doc.PageSize.Width - 60f, doc.PageSize.Height - 100f);
//            column2.SetSimpleColumn(200f, doc.PageSize.Height - 40f, doc.PageSize.Width - 80f, doc.PageSize.Height - 120f);
//            Paragraph p = new Paragraph()
//            {
//                Alignment = Element.ALIGN_CENTER
//            };
//            Paragraph p1 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_RIGHT
//            };

//            Paragraph p2 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_LEFT
//            };

//            Paragraph p3 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_CENTER
//            };

//            p.Add(c1);
//            p.Add(c2);
//            p.Add(c3);
//            p.Add(c4);
//            p.Add(c5);
//            p.Add(c6);
//            column1.AddElement(p);
//            column1.AddElement(tabla1);
//            column1.AddElement(c7);
//            p2.Add(c8);
//            p2.Add(c9);
//            p2.Add(c10);
//            p2.Add(c11);
//            p2.Add(c12);
//            p2.Add(c13);
//            p2.Add(c14);
//            p2.Add(c15);
//            p2.Add(c16);
//            p2.Add(c17);
//            p2.Add(c21);
//            p2.Add(c22);
//            p2.Add(c23);
//            p2.Add(c24);
//            p2.Add(c25);
//            p2.Add(c26);
//            column1.AddElement(p2);
//            column1.Go();
//            p1.Add(c18);
//            p1.Add(c19);
//            p1.Add(c20);
//            column2.AddElement(p1);
//            column2.Go();

//            //Pagina 2
//            doc.NewPage();

//            ColumnText column3 = new ColumnText(cb);
//            column3.SetSimpleColumn(60f, doc.PageSize.Height - 1500f, doc.PageSize.Width - 60f, doc.PageSize.Height - 20f);

//            Paragraph p5 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_LEFT
//            };
//            p5.Add(c27);
//            p5.Add(c28);
//            p5.Add(c29);
//            p5.Add(c30);
//            column3.AddElement(p5);
//            p3.Add(c31);
//            p3.Add(c32);
//            column3.AddElement(p3);
//            column3.AddElement(p4);
//            column3.Go();

//            ColumnText column4 = new ColumnText(cb);
//            column4.SetSimpleColumn(doc.PageSize.Width - 230f, doc.PageSize.Height - 2400f, doc.PageSize.Width - 70f, doc.PageSize.Height - 500f);
//            Paragraph p6 = new Paragraph()
//            {
//                Alignment = Element.ALIGN_JUSTIFIED
//            };

//            p6.Add(c33);
//            p6.Add(c34);
//            p6.Add(c35);
//            column4.AddElement(p6);
//            column4.Go();

//            doc.Close();
//        }
//        public void notebook()
//        {
//            Document doc = new Document(iTextSharp.text.PageSize.LETTER.Rotate(), 10, 10, 42, 35);
//            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("GUA-DGAM-LIB-" + CorrelativeNotebook.Text + "-2017.pdf", FileMode.Create));
//            doc.Open();
//            PdfContentByte cb = wri.DirectContent;

//            doc.NewPage();

//            ColumnText column1 = new ColumnText(cb);
//            column1.SetSimpleColumn(35, 30, 35 * 10, 30 * 18);

//            Paragraph p = new Paragraph("ESTE LIBRO SE EXTIENDE CON EL PROPOSITO DE REGISTRAR LA CAPACITACION Y DESEMPEÑO LABORAL DE LA GENTE DE MAR, Y COMPROBAR SU IDONEIDAD Y EXPERIENCIA LABORAL EN LAS FUNCIONES PROPIAS DE LAS TRIPULACIONES DE BUQUES MERCANTES."
//            + "\n\n\nThis book is issued with the purpose of record the bearer’s training and sea service, as a mean to prove the seafarer’s experience in the proper functions of a merchant ship’s crew."
//            + "\n\n\nLOS REGISTROS QUE EN ESTE LIBRO SE ANOTEN, DEBEN ESTAR PLENAMENTE RESPALDADOS CON LOS RESPECTIVOS TITULOS, SIENDO RESPONSABLE DE SU VERIFICACION, LA ENTIDAD O AUTORIDAD QUE HAGA EL REGISTRO."
//            + "\n\n\nRecords in this book have to be backed up with the proper certificates, the authority who fills the record will be responsible for its proper verification."
//            + "\n\n\nESTE LIBRO NO ES UN PASAPORTE, NI DOCUMENTO DE IDENTIDAD DEL PORTADOR, SE EMITE SIN EL PREJUICIO A Y DE NINGUNA MANERA  AFECTA LA NACIONALIDAD DEL POSEEDOR."
//            + "\n\n\nThis book is neither a passport nor identification document of the bearer, and it is issued without prejudice to and in no way affects the national status of the bearer.");
//            p.Alignment = Element.ALIGN_JUSTIFIED;
//            var titleFont = FontFactory.GetFont("Arial", 7.5f, BaseColor.BLACK);
//            p.Font = titleFont;
//            column1.AddElement(p);
//            column1.Go();

//            ColumnText column2 = new ColumnText(cb);
//            column2.SetSimpleColumn(385, 30, 385 + (35 * 11), 30 * 18);

//            Paragraph p2 = new Paragraph("SI ESTE LIBRO SE LLENA DE INFORMACION, SE DAÑA, ES ROBADO, PERDIDO, O ACCIDENTALMENTE DESTRUIDO, LA SOLICITUD DEBE HACERSE INMEDIATAMENTE A LA DIRECCIÓN GENERAL DE ASUNTOS MARÍTIMOS DEL MINISTERIO DE LA DEFENSA NACIONAL PARA SU REEMPLAZO, UNA VEZ EL LIBRO NO HA EXPIRADO, CASO CONTRARIO EL PROPIETARIO RETENDRÁ POSESIÓN DE ÉL PARA COMPROBAR SU IDONEIDAD Y EXPERIENCIA."
//            + "\n\n\nIf this book becomes damaged, an application for its replacement should be made immediately and addressed to the General Maritime Affairs Division Ministry Of National Defense.Once the book has expired the seafarer will retain possession of it for proof of sea service."
//            + "\n\n\nESTE LIBRO NO DEBE ALTERARSE DE FORMA ALGUNA Y NO PUEDE SER TRANSFERIBLE A OTRA PERSONA.CUALQUIER PERSONA.QUE LO ENCUENTRE  DEBE ENVIARLO A LA AUTORIDAD MARITIMA NACIONAL DE GUATEMALA."
//            + "\n\n\nThis book may not be altered in any way and is not transferable to any other person.Any person finding this book should send it to the Guatemala Maritime National Authority."
//            + "\n\n\n\nDirección General de Asuntos Marítimos"
//            + "\nMinisterio de la Defensa Nacional"
//            + "\nGuatemala");
//            p2.Alignment = Element.ALIGN_JUSTIFIED;
//            var titleFont2 = FontFactory.GetFont("Arial", 7, BaseColor.BLACK);
//            p2.Font = titleFont2;
//            column2.AddElement(p2);
//            column2.Go();

//            doc.NewPage();

//            ColumnText column3 = new ColumnText(cb);
//            column3.SetSimpleColumn(35, 30, 35 * 10, 30 * 18);
//            Paragraph p3 = new Paragraph("OBSERVACIONES\nObservations\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nPAGINA 12\nPAGE 12");
//            p3.Alignment = Element.ALIGN_CENTER;
//            p3.Font = titleFont2;
//            column3.AddElement(p3);
//            column3.Go();

//            ColumnText column4 = new ColumnText(cb);
//            column4.SetSimpleColumn(385, 30, 385 + (35 * 11), 30 * 18);

//            Paragraph p4 = new Paragraph("REPUBLICA DE GUATEMALA"
//            + "\nGUATEMALA REPUBLIC"
//            + "\n\nDIRECCIÓN DE ASUNTOS MARÍTIMOS"
//            + "\n\nGENERAL MARITIME AFFAIRS DIVISION"
//            + "\n\nLIBRO DE REGISTRO DE IDONEIDAD LABORAL PERTENECIENTE A:"
//            + "\nTHIS SEAFARER’S COMPETENCE DOCUMENT AND RECORD BOOK IS GRANTED TO:"
//            + "\n\n" + PersonName.Text + PersonLastName.Text
//            + "\n\nEXPEDIDO EN BASE A LA FACULTAD CONFERIDA POR EL GOBIERNO DE LA REPÚBLICA POR MEDIO DEL ACUERDO GUBERNATIVO NO. 65-2014"
//            + "\n WITH THE FACULTY CONFERED BY THE GOVERNMENT THROUGH GOVERNMENTAL AGREEMENT 65-2014 ISSUES THE PRESENT CERTIFICATE BY:"
//            + "\n______________________________"
//            + "\nDIRECTOR GENERAL DE ASUNTOS MARITIMOS"
//            + "\nGUATEMALA MARITME DEPARTMENT DIRECTOR"
//            + "\n\nESTE LIBRO CONTIENE 12 PÁGINAS NUMERADAS EXCLUYENDO LAS PORTADAS"
//            + "\nTHE BOOK CONTAINS 12 NUMBERED PAGES EXCLUDING COVERS"
//            + "\n\nPAGINA 1"
//            + "\nPAGE 1");

//            p4.Alignment = Element.ALIGN_CENTER;
//            titleFont2 = FontFactory.GetFont("Arial", 9, BaseColor.BLACK);
//            p4.Font = titleFont2;
//            column4.AddElement(p4);
//            column4.Go();

//            doc.NewPage();

//            ColumnText column5 = new ColumnText(cb);
//            column5.SetSimpleColumn(35, 30, 35 * 10, 30 * 18);

//            PdfPTable tabla = new PdfPTable(4);
//            Phrase f1 = new Phrase("EXTENDIDO A / Issue To \n APELLIDO / Surname: " + PersonName.Text + " \n NOMBRES / First Name: " + PersonName.Text);
//            PdfPCell titulo = new PdfPCell(f1);
//            titulo.Colspan = 4;
//            titulo.HorizontalAlignment = 0;
//            titulo.FixedHeight = 40f;
//            tabla.AddCell(titulo);

//            Phrase f2 = new Phrase("Fecha de Nacimiento \n Birth Date \n" + Birthday.Text);
//            PdfPCell titulo2 = new PdfPCell(f2);
//            titulo2.Colspan = 1;
//            titulo2.HorizontalAlignment = 0;
//            titulo2.FixedHeight = 40f;
//            tabla.AddCell(titulo2);

//            Phrase f3 = new Phrase("Lugar de Nacimiento NACIONALIDAD   Birthplace Nationality\n " + BirthPlace.Text);
//            PdfPCell titulo3 = new PdfPCell(f3);
//            titulo3.Colspan = 3;
//            titulo3.HorizontalAlignment = 0;
//            titulo3.FixedHeight = 40f;
//            tabla.AddCell(titulo3);

//            Phrase f4 = new Phrase("COLOR DE CABELLO Hair Color \n " + HairColor.Text);
//            PdfPCell titulo4 = new PdfPCell(f4);
//            titulo4.Colspan = 1;
//            titulo4.HorizontalAlignment = 0;
//            titulo4.FixedHeight = 40f;
//            tabla.AddCell(titulo4);

//            Phrase f5 = new Phrase("ESTATURA / Height \n " + Height.Text);
//            PdfPCell titulo5 = new PdfPCell(f5);
//            titulo5.Colspan = 1;
//            titulo5.HorizontalAlignment = 0;
//            titulo5.FixedHeight = 40f;
//            tabla.AddCell(titulo5);

//            Phrase f6 = new Phrase("PESO / Weight \n " + Weight.Text);
//            PdfPCell titulo6 = new PdfPCell(f6);
//            titulo6.Colspan = 1;
//            titulo6.HorizontalAlignment = 0;
//            titulo6.FixedHeight = 40f;
//            tabla.AddCell(titulo6);

//            if (MaleCheck.Checked == true)
//            {
//                Phrase f7 = new Phrase("SEXO / Sex \n Hombre / Male");
//                PdfPCell titulo7 = new PdfPCell(f7);
//                titulo7.Colspan = 1;
//                titulo7.HorizontalAlignment = 0;
//                titulo7.FixedHeight = 40f;
//                tabla.AddCell(titulo7);
//            }
//            else if (FemaleCheck.Checked == true)
//            {
//                Phrase f7 = new Phrase("SEXO / Sex \n Mujer / Female");
//                PdfPCell titulo7 = new PdfPCell(f7);
//                titulo7.Colspan = 1;
//                titulo7.HorizontalAlignment = 0;
//                titulo7.FixedHeight = 40f;
//                tabla.AddCell(titulo7);
//            }
//            else
//            {
//                Phrase f7 = new Phrase("SEXO / Sex \n Na");
//                PdfPCell titulo7 = new PdfPCell(f7);
//                titulo7.Colspan = 1;
//                titulo7.HorizontalAlignment = 0;
//                titulo7.FixedHeight = 40f;
//                tabla.AddCell(titulo7);
//            }

//            Phrase f8 = new Phrase("COLOR DE OJOS Eye’s color \n " + EyeColor.Text);
//            PdfPCell titulo8 = new PdfPCell(f8);
//            titulo8.Colspan = 1;
//            titulo8.HorizontalAlignment = 0;
//            titulo8.FixedHeight = 40f;
//            tabla.AddCell(titulo8);

//            Phrase f9 = new Phrase("DOCUMENTO DE IDENTIFICACIÓN Identification Document \n DPI: " + DPI.Text);
//            PdfPCell titulo9 = new PdfPCell(f9);
//            titulo9.Colspan = 3;
//            titulo9.HorizontalAlignment = 0;
//            titulo9.FixedHeight = 40f;
//            tabla.AddCell(titulo9);

//            Phrase f10 = new Phrase("MARCAS DISTINTIVAS Distinguishing Marks \n xxxxxxxxxxxxxxx");
//            PdfPCell titulo10 = new PdfPCell(f10);
//            titulo10.Colspan = 2;
//            titulo10.HorizontalAlignment = 0;
//            titulo10.FixedHeight = 40f;
//            tabla.AddCell(titulo10);

//            Phrase f11 = new Phrase("OCUPACIÓN : Occupation: \n xxxxxxxxxxxx");
//            PdfPCell titulo11 = new PdfPCell(f11);
//            titulo11.Colspan = 2;
//            titulo11.HorizontalAlignment = 0;
//            titulo11.FixedHeight = 40f;
//            tabla.AddCell(titulo11);

//            Phrase f12 = new Phrase("FECHA DE EXTENSIÓN: Date of issue");
//            PdfPCell titulo12 = new PdfPCell(f12);
//            titulo12.Colspan = 1;
//            titulo12.HorizontalAlignment = 0;
//            titulo12.FixedHeight = 40f;
//            tabla.AddCell(titulo12);

//            Phrase f13 = new Phrase(monthCalendar2.SelectionStart.ToShortDateString());
//            PdfPCell titulo13 = new PdfPCell(f13);
//            titulo13.Colspan = 1;
//            titulo13.HorizontalAlignment = 0;
//            titulo13.FixedHeight = 40f;
//            tabla.AddCell(titulo13);

//            Phrase f14 = new Phrase("FECHA DE VENCIMIENTO:  Expiration Date");
//            PdfPCell titulo14 = new PdfPCell(f14);
//            titulo14.Colspan = 1;
//            titulo14.HorizontalAlignment = 0;
//            titulo14.FixedHeight = 40f;
//            tabla.AddCell(titulo14);

//            Phrase f15 = new Phrase(monthCalendar3.SelectionStart.ToShortDateString());
//            PdfPCell titulo15 = new PdfPCell(f15);
//            titulo15.Colspan = 1;
//            titulo15.HorizontalAlignment = 0;
//            titulo15.FixedHeight = 40f;
//            tabla.AddCell(titulo15);

//            Phrase f16 = new Phrase("LUGAR DE EXTENSIÓN Place of Issue : CIUDAD DE GUATEMALA");
//            PdfPCell titulo16 = new PdfPCell(f16);
//            titulo16.Colspan = 4;
//            titulo16.HorizontalAlignment = 0;
//            titulo16.FixedHeight = 40f;
//            tabla.AddCell(titulo16);

//            tabla.WidthPercentage = 100;

//            Paragraph p5 = new Paragraph("\nPAGINA 2\nPAGE 2");
//            p5.Alignment = Element.ALIGN_CENTER;
//            p5.Font = titleFont2;

//            column5.AddElement(tabla);
//            column5.Go();

//            ColumnText column6 = new ColumnText(cb);
//            column6.SetSimpleColumn(385, 30, 385 + (35 * 11), 30 * 18);
//            Paragraph p6 = new Paragraph("OBSERVACIONES\nObservations\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nPAGINA 11\nPAGE 11");
//            p6.Alignment = Element.ALIGN_CENTER;
//            p6.Font = titleFont2;
//            column6.AddElement(p6);
//            column6.Go();

//            doc.NewPage();

//            ColumnText column7 = new ColumnText(cb);
//            column7.SetSimpleColumn(35, 30, 35 * 10, 30 * 18);
//            Paragraph p7 = new Paragraph("OBSERVACIONES\nObservations\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nPAGINA 10\nPAGE 10");
//            p7.Alignment = Element.ALIGN_CENTER;
//            p7.Font = titleFont2;
//            column7.AddElement(p7);
//            column7.Go();

//            ColumnText column8 = new ColumnText(cb);
//            column8.SetSimpleColumn(385, 30, 385 + (35 * 11), 30 * 18);

//            Paragraph p8 = new Paragraph("FOTOGRAFIA"
//            + "\n\n\n\n\nDirección General de Asuntos Marítimos"
//            + "\n\nAutoridad Marítima");

//            p8.Alignment = Element.ALIGN_CENTER;
//            p8.Font = titleFont2;
//            column8.AddElement(p8);
//            column8.Go();

//            doc.Close();
//        }
//    }
//}
