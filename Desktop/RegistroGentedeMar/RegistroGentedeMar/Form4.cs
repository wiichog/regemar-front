﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace RegistroGentedeMar
{
    public partial class Form4 : Form
    {
        public string value = "";
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            MySQL_ToDatagridview();
        }
        private void MySQL_ToDatagridview()
        {
            encrypt en = new encrypt();
            string connectionString = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
            string sql = "SELECT `DPI`,`nombre`,`apellido` FROM `persona`";
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            MySqlCommand sCommand = new MySqlCommand(sql, connection);
            MySqlDataAdapter sAdapter = new MySqlDataAdapter(sCommand);
            MySqlCommandBuilder sBuilder = new MySqlCommandBuilder(sAdapter);
            DataSet sDs = new DataSet();
            sAdapter.Fill(sDs, "persons");
            DataTable sTable = sDs.Tables["persons"];
            connection.Close();
            dataGridView1.DataSource = sDs.Tables["persons"];
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
   
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dataGridView1.CurrentCell.OwningRow;
            value = row.Cells["DPI"].Value.ToString();
            var edit = new Form5(value);
            edit.Visible = true;
            this.Visible = false;
        }
        public string dpiValue
        {
            get { return value; }
        }
    }
}
