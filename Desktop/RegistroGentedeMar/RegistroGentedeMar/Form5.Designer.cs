﻿namespace RegistroGentedeMar
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label18 = new System.Windows.Forms.Label();
            this.NewPerson = new System.Windows.Forms.Button();
            this.TakePicture = new System.Windows.Forms.Button();
            this.PersonPicture = new System.Windows.Forms.PictureBox();
            this.PersonLastName = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.PersonName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.Birthday = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PersonState = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.PersonCountry = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.PersonNationality = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.FemaleCheck = new System.Windows.Forms.CheckBox();
            this.MaleCheck = new System.Windows.Forms.CheckBox();
            this.Email = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.PersonalPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.HomePhone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.EyeColor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.HairColor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Height = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BirthPlace = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.personDPI = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewLinkColumn();
            this.idCourse = new System.Windows.Forms.DataGridViewLinkColumn();
            this.SubirCertificadoFirmado = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BajarCertificadoNOFirmado = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BajarCertificadoFirmado = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PersonPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(181, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(185, 20);
            this.label18.TabIndex = 93;
            this.label18.Text = "Edicion de Gente de Mar";
            // 
            // NewPerson
            // 
            this.NewPerson.Location = new System.Drawing.Point(610, 360);
            this.NewPerson.Name = "NewPerson";
            this.NewPerson.Size = new System.Drawing.Size(154, 23);
            this.NewPerson.TabIndex = 89;
            this.NewPerson.Text = "Editar Persona";
            this.NewPerson.UseVisualStyleBackColor = true;
            this.NewPerson.Click += new System.EventHandler(this.NewPerson_Click);
            // 
            // TakePicture
            // 
            this.TakePicture.Location = new System.Drawing.Point(610, 331);
            this.TakePicture.Name = "TakePicture";
            this.TakePicture.Size = new System.Drawing.Size(154, 23);
            this.TakePicture.TabIndex = 86;
            this.TakePicture.Text = "Cambiar Foto";
            this.TakePicture.UseVisualStyleBackColor = true;
            this.TakePicture.Click += new System.EventHandler(this.TakePicture_Click);
            // 
            // PersonPicture
            // 
            this.PersonPicture.Location = new System.Drawing.Point(493, 49);
            this.PersonPicture.Name = "PersonPicture";
            this.PersonPicture.Size = new System.Drawing.Size(271, 275);
            this.PersonPicture.TabIndex = 82;
            this.PersonPicture.TabStop = false;
            // 
            // PersonLastName
            // 
            this.PersonLastName.Location = new System.Drawing.Point(137, 75);
            this.PersonLastName.Name = "PersonLastName";
            this.PersonLastName.Size = new System.Drawing.Size(275, 20);
            this.PersonLastName.TabIndex = 60;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(20, 78);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(49, 13);
            this.Label2.TabIndex = 59;
            this.Label2.Text = "Apellidos";
            // 
            // PersonName
            // 
            this.PersonName.Location = new System.Drawing.Point(137, 49);
            this.PersonName.Name = "PersonName";
            this.PersonName.Size = new System.Drawing.Size(275, 20);
            this.PersonName.TabIndex = 58;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Nombre";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(610, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 23);
            this.button1.TabIndex = 94;
            this.button1.Text = "Menu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.monthCalendar1.Location = new System.Drawing.Point(168, 139);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 97;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // Birthday
            // 
            this.Birthday.Enabled = false;
            this.Birthday.Location = new System.Drawing.Point(137, 107);
            this.Birthday.Name = "Birthday";
            this.Birthday.Size = new System.Drawing.Size(275, 20);
            this.Birthday.TabIndex = 96;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 95;
            this.label3.Text = "Fecha de Nacimiento";
            // 
            // PersonState
            // 
            this.PersonState.Location = new System.Drawing.Point(137, 453);
            this.PersonState.Name = "PersonState";
            this.PersonState.Size = new System.Drawing.Size(275, 20);
            this.PersonState.TabIndex = 125;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(21, 459);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 13);
            this.label26.TabIndex = 124;
            this.label26.Text = "Departamento";
            // 
            // PersonCountry
            // 
            this.PersonCountry.Location = new System.Drawing.Point(137, 427);
            this.PersonCountry.Name = "PersonCountry";
            this.PersonCountry.Size = new System.Drawing.Size(275, 20);
            this.PersonCountry.TabIndex = 123;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 430);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 13);
            this.label25.TabIndex = 122;
            this.label25.Text = "Pais de Origen";
            // 
            // PersonNationality
            // 
            this.PersonNationality.Location = new System.Drawing.Point(137, 401);
            this.PersonNationality.Name = "PersonNationality";
            this.PersonNationality.Size = new System.Drawing.Size(275, 20);
            this.PersonNationality.TabIndex = 121;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(20, 403);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 13);
            this.label24.TabIndex = 120;
            this.label24.Text = "Nacionalidad";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 495);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 119;
            this.label14.Text = "Sexo";
            // 
            // FemaleCheck
            // 
            this.FemaleCheck.AutoSize = true;
            this.FemaleCheck.Location = new System.Drawing.Point(296, 495);
            this.FemaleCheck.Name = "FemaleCheck";
            this.FemaleCheck.Size = new System.Drawing.Size(72, 17);
            this.FemaleCheck.TabIndex = 118;
            this.FemaleCheck.Text = "Femenino";
            this.FemaleCheck.UseVisualStyleBackColor = true;
            // 
            // MaleCheck
            // 
            this.MaleCheck.AutoSize = true;
            this.MaleCheck.Location = new System.Drawing.Point(194, 495);
            this.MaleCheck.Name = "MaleCheck";
            this.MaleCheck.Size = new System.Drawing.Size(74, 17);
            this.MaleCheck.TabIndex = 117;
            this.MaleCheck.Text = "Masculino";
            this.MaleCheck.UseVisualStyleBackColor = true;
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(137, 682);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(275, 20);
            this.Email.TabIndex = 116;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 685);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 115;
            this.label13.Text = "Correo Electronico";
            // 
            // PersonalPhone
            // 
            this.PersonalPhone.Location = new System.Drawing.Point(137, 656);
            this.PersonalPhone.Name = "PersonalPhone";
            this.PersonalPhone.Size = new System.Drawing.Size(275, 20);
            this.PersonalPhone.TabIndex = 114;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 659);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 13);
            this.label12.TabIndex = 113;
            this.label12.Text = "Telefono Personal";
            // 
            // HomePhone
            // 
            this.HomePhone.Location = new System.Drawing.Point(137, 630);
            this.HomePhone.Name = "HomePhone";
            this.HomePhone.Size = new System.Drawing.Size(275, 20);
            this.HomePhone.TabIndex = 112;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 633);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 111;
            this.label11.Text = "Telefono de Casa";
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(137, 604);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(275, 20);
            this.Address.TabIndex = 110;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 607);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 109;
            this.label10.Text = "Direccion";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 581);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 108;
            this.label9.Text = "DPI";
            // 
            // EyeColor
            // 
            this.EyeColor.Location = new System.Drawing.Point(137, 552);
            this.EyeColor.Name = "EyeColor";
            this.EyeColor.Size = new System.Drawing.Size(275, 20);
            this.EyeColor.TabIndex = 107;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 555);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 106;
            this.label8.Text = "Color de Ojos";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // HairColor
            // 
            this.HairColor.Location = new System.Drawing.Point(137, 526);
            this.HairColor.Name = "HairColor";
            this.HairColor.Size = new System.Drawing.Size(275, 20);
            this.HairColor.TabIndex = 105;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 529);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 104;
            this.label7.Text = "Color de Cabello";
            // 
            // Weight
            // 
            this.Weight.Location = new System.Drawing.Point(137, 372);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(275, 20);
            this.Weight.TabIndex = 103;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 375);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 102;
            this.label6.Text = "Peso (lb)";
            // 
            // Height
            // 
            this.Height.Location = new System.Drawing.Point(137, 346);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(275, 20);
            this.Height.TabIndex = 101;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 349);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 100;
            this.label5.Text = "Estatura (m)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // BirthPlace
            // 
            this.BirthPlace.Location = new System.Drawing.Point(137, 320);
            this.BirthPlace.Name = "BirthPlace";
            this.BirthPlace.Size = new System.Drawing.Size(275, 20);
            this.BirthPlace.TabIndex = 99;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 323);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 98;
            this.label4.Text = "Lugar de Nacimiento";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.idCourse,
            this.SubirCertificadoFirmado,
            this.BajarCertificadoNOFirmado,
            this.BajarCertificadoFirmado});
            this.dataGridView1.Location = new System.Drawing.Point(430, 453);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(334, 287);
            this.dataGridView1.TabIndex = 126;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // personDPI
            // 
            this.personDPI.Location = new System.Drawing.Point(137, 578);
            this.personDPI.Name = "personDPI";
            this.personDPI.Size = new System.Drawing.Size(275, 20);
            this.personDPI.TabIndex = 127;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(444, 425);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(308, 20);
            this.label15.TabIndex = 128;
            this.label15.Text = "Carga de Certificados Firmados de Cursos";
            // 
            // Column1
            // 
            this.Column1.ActiveLinkColor = System.Drawing.Color.Black;
            this.Column1.DataPropertyName = "nombreCurso";
            this.Column1.HeaderText = "Curso";
            this.Column1.LinkColor = System.Drawing.Color.Black;
            this.Column1.Name = "Column1";
            this.Column1.VisitedLinkColor = System.Drawing.Color.Black;
            this.Column1.Width = 40;
            // 
            // idCourse
            // 
            this.idCourse.DataPropertyName = "idCurso";
            this.idCourse.HeaderText = "Id del Curso";
            this.idCourse.Name = "idCourse";
            this.idCourse.Visible = false;
            this.idCourse.Width = 69;
            // 
            // SubirCertificadoFirmado
            // 
            this.SubirCertificadoFirmado.HeaderText = "Subir Certificado Firmado";
            this.SubirCertificadoFirmado.Name = "SubirCertificadoFirmado";
            this.SubirCertificadoFirmado.Width = 117;
            // 
            // BajarCertificadoNOFirmado
            // 
            this.BajarCertificadoNOFirmado.HeaderText = "Bajar Certificado NO Firmado";
            this.BajarCertificadoNOFirmado.Name = "BajarCertificadoNOFirmado";
            this.BajarCertificadoNOFirmado.Width = 101;
            // 
            // BajarCertificadoFirmado
            // 
            this.BajarCertificadoFirmado.HeaderText = "Bajar Certificado Firmado";
            this.BajarCertificadoFirmado.Name = "BajarCertificadoFirmado";
            this.BajarCertificadoFirmado.Width = 117;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 752);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.personDPI);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.PersonState);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.PersonCountry);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.PersonNationality);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.FemaleCheck);
            this.Controls.Add(this.MaleCheck);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.PersonalPhone);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.HomePhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.EyeColor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.HairColor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Weight);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Height);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BirthPlace);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.Birthday);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.NewPerson);
            this.Controls.Add(this.TakePicture);
            this.Controls.Add(this.PersonPicture);
            this.Controls.Add(this.PersonLastName);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.PersonName);
            this.Controls.Add(this.label1);
            this.Name = "Form5";
            this.Text = "Form5";
            this.Load += new System.EventHandler(this.Form5_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PersonPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button NewPerson;
        private System.Windows.Forms.Button TakePicture;
        private System.Windows.Forms.PictureBox PersonPicture;
        private System.Windows.Forms.TextBox PersonLastName;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.TextBox PersonName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TextBox Birthday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PersonState;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox PersonCountry;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox PersonNationality;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox FemaleCheck;
        private System.Windows.Forms.CheckBox MaleCheck;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox PersonalPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox HomePhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox EyeColor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox HairColor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Weight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Height;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox BirthPlace;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox personDPI;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewLinkColumn Column1;
        private System.Windows.Forms.DataGridViewLinkColumn idCourse;
        private System.Windows.Forms.DataGridViewButtonColumn SubirCertificadoFirmado;
        private System.Windows.Forms.DataGridViewButtonColumn BajarCertificadoNOFirmado;
        private System.Windows.Forms.DataGridViewButtonColumn BajarCertificadoFirmado;
    }
}