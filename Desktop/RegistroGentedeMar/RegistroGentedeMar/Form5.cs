﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Net;
using System.IO;

namespace RegistroGentedeMar
{
    public partial class Form5 : Form
    {
        public string DPI = "";
        public String ImageSource = "";
        public string PicturePathToServer = "";
        public String Signedcertificate = "";
        public Form5(string s)
        {
            InitializeComponent();
            DPI = s;
            BringInitialInformation(DPI);
            BringCourses();
            SetPersonImage(DPI);
        }

        private void Form5_Load(object sender, EventArgs e)
        {
        }

        public void BringCourses() {
            MySqlConnection mysqlCon;
            encrypt en = new encrypt();
            mysqlCon = new MySqlConnection(en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM="));
            mysqlCon.Open();
            MySqlDataAdapter MyDA = new MySqlDataAdapter();
            string sqlSelectAll = "SELECT * FROM `cursos`";
            MyDA.SelectCommand = new MySqlCommand(sqlSelectAll, mysqlCon);
            DataTable table = new DataTable();
            MyDA.Fill(table);

            BindingSource bSource = new BindingSource();
            bSource.DataSource = table;


            dataGridView1.DataSource = bSource;


        }
        public void BringInitialInformation(String DPI) {
            encrypt en = new encrypt();
            string connetionString = null;
            MySqlConnection cnn;
            connetionString = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
            cnn = new MySqlConnection(connetionString);
            try
            {
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT `DPI`,`nombre`,`apellido`,`fechaNacimiento`,`nacionalidad`," +
                    "`pais`,`departamento`,`estatura`,`peso`,`sexo`,`colorCabello`,`colorOjos`,`direccion`,`telefonoCasa`" +
                    ",`telefonoCelular`,`correoElectronico`,`fotoPersona` FROM `persona` WHERE DPI = '" + DPI + "'", cnn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PersonName.Text = reader.GetValue(1).ToString();
                    PersonLastName.Text = reader.GetValue(2).ToString();
                    Birthday.Text = reader.GetValue(3).ToString();
                    PersonNationality.Text = reader.GetValue(4).ToString();
                    PersonCountry.Text = reader.GetValue(5).ToString();
                    PersonState.Text = reader.GetValue(6).ToString();
                    Height.Text = reader.GetValue(7).ToString();
                    Weight.Text = reader.GetValue(8).ToString();
                    HairColor.Text = reader.GetValue(10).ToString();
                    EyeColor.Text = reader.GetValue(11).ToString();
                    Address.Text = reader.GetValue(12).ToString();
                    HomePhone.Text = reader.GetValue(13).ToString();
                    PersonalPhone.Text = reader.GetValue(14).ToString();
                    Email.Text = reader.GetValue(15).ToString();
                    personDPI.Text = DPI;
                    PicturePathToServer = reader.GetValue(16).ToString();
                    if (reader.GetValue(9).ToString().Equals("M"))
                    {
                        MaleCheck.Checked = true;
                        FemaleCheck.Checked = false;
                    }
                    else {
                        MaleCheck.Checked = false;
                        FemaleCheck.Checked = true;
                    }
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.Show(string.Concat("Error: ", ex.ToString(), " ENVIAR ESTE ERROR AL EQUIPO DE DESARROLLO"));
            }

            
        }

        public void SetPersonImage(string DPI)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://198.49.72.66/persons/" + DPI + "/" + DPI + "Picture.jpg");
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            Console.Write("HJ");
            request.Credentials = new NetworkCredential("regemaruvg@dgam.gob.gt", "regemaruvg123");

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            PersonPicture.Image = Image.FromStream(responseStream);
            PersonPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            reader.Close();
            response.Close();
        }


        public void editInformation(String Name, String LastName, String Birthday, String PlaceBirth, String Height,
            String Weight, String Nacionality, String Country, String State, String Sex, String HairColor, String EyesColor,
            String DPI, String Address, String HomePhone, String PersonalPhone, String Email)
        {
            try
            {
                encrypt en = new encrypt();
                string MyConnection2 = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
                string Query = "UPDATE `persona` SET `nombre`='" + Name + "',`apellido`='" + LastName + "'," +
                    "`fechaNacimiento`='" + Birthday + "',`nacionalidad`='" + Nacionality + "',`pais`='" + Country + "'" +
                    ",`departamento`='" + State + "'," +
                    "`estatura`='" + Height + "',`peso`='" + Weight + "',`sexo`='" + Sex + "'," +
                    "`colorCabello`='" + HairColor + "',`colorOjos`='" + EyesColor + "'," +
                    "`direccion`='" + Address + "',`telefonoCasa`='" + HomePhone + "',`telefonoCelular`='" + PersonalPhone + "'," +
                    "`correoElectronico`='" + Email + "' WHERE DPI = '" + DPI + "'";
                Console.Write(Query);
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void deletePicture() {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://198.49.72.66/persons/" + DPI + "/" + DPI + "Picture.jpg");
            request.Credentials = new NetworkCredential("regemaruvg@dgam.gob.gt", "regemaruvg123");
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Console.WriteLine("Delete status: {0}", response.StatusDescription);
            response.Close();
        }

        private void uploadFile(string fileSource, String DPI, String name, String type)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://198.49.72.66/persons/" + DPI + "/" + DPI + name + type);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials =
            new NetworkCredential("regemaruvg@dgam.gob.gt", "regemaruvg123");
            byte[] bytes = System.IO.File.ReadAllBytes(fileSource);
            request.ContentLength = bytes.Length;
            using (Stream request_stream = request.GetRequestStream())
            {
                request_stream.Write(bytes, 0, bytes.Length);
                request_stream.Close();
            }
        }


        private void NewPerson_Click(object sender, EventArgs e)
        {
            string Gender = "";
            if (MaleCheck.Checked == true)
            {
                Gender = "M";
            }
            else if (FemaleCheck.Checked == true)
            {
                Gender = "F";
            }
            editInformation(PersonName.Text, PersonLastName.Text, Birthday.Text, BirthPlace.Text, Height.Text, Weight.Text,
                PersonNationality.Text, PersonCountry.Text, PersonState.Text, Gender, HairColor.Text, EyeColor.Text, personDPI.Text,
                Address.Text, HomePhone.Text, PersonalPhone.Text, Email.Text);
           
        }

        private void TakePicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String path = dialog.FileName;
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), new UTF8Encoding()))
                {
                    PersonPicture.ImageLocation = path;
                    PersonPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                    ImageSource = path;
                    reader.Close();
                }
            }
            deletePicture();
            uploadFile(ImageSource, DPI, "Picture", ".jpg");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            form4.Visible = true;
            this.Visible = false;
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            var monthCalendar = sender as MonthCalendar;
            Birthday.Text = monthCalendar1.SelectionStart.ToShortDateString();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["SubirCertificadoFirmado"].Index && e.RowIndex >= 0)
            {
                saveUpload upload = new saveUpload();
                ftpserver ftp = new ftpserver();
                Signedcertificate = upload.upload(Signedcertificate);
                try
                {
                    ftp.uploadFile(Signedcertificate, personDPI.Text, dataGridView1.Rows[0].Cells["Column1"].Value.ToString() + "SIGNED", ".pdf");
                    MessageBox.Show(string.Concat("Documentos Subido Exitosamente"));
                }
                catch (Exception error) {
                    MessageBox.Show(string.Concat("Documentos Subido Exitosamente"));
                }
            }
            if (e.ColumnIndex == dataGridView1.Columns["BajarCertificadoNOFirmado"].Index && e.RowIndex >= 0)
            {
                
            }
            if (e.ColumnIndex == dataGridView1.Columns["BajarCertificadoFirmado"].Index && e.RowIndex >= 0)
            {
              
            }
        }
    }
}
    