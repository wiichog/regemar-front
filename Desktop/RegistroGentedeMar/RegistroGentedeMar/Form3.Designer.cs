﻿namespace RegistroGentedeMar
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PersonName = new System.Windows.Forms.TextBox();
            this.PersonLastName = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Birthday = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BirthPlace = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Height = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.HairColor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.EyeColor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DPI = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.HomePhone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.PersonalPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.PersonPicture = new System.Windows.Forms.PictureBox();
            this.MaleCheck = new System.Windows.Forms.CheckBox();
            this.FemaleCheck = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TakePicture = new System.Windows.Forms.Button();
            this.UploadDPI = new System.Windows.Forms.Button();
            this.UploadMedic = new System.Windows.Forms.Button();
            this.error = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.GenerateNotebook = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.monthCalendar2 = new System.Windows.Forms.MonthCalendar();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.monthCalendar3 = new System.Windows.Forms.MonthCalendar();
            this.GenerateCertificate = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.UploadDiplomaCourse = new System.Windows.Forms.Button();
            this.GenerateMedics = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.CourseCorrelative = new System.Windows.Forms.TextBox();
            this.CorrelativeMedics = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.CorrelativeNotebook = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.NameOfCourse = new System.Windows.Forms.ComboBox();
            this.NewPerson = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Menu = new System.Windows.Forms.Button();
            this.PersonNationality = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.PersonCountry = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.PersonState = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.CertificateRequest = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PersonPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // PersonName
            // 
            this.PersonName.Location = new System.Drawing.Point(154, 36);
            this.PersonName.Name = "PersonName";
            this.PersonName.Size = new System.Drawing.Size(312, 20);
            this.PersonName.TabIndex = 1;
            // 
            // PersonLastName
            // 
            this.PersonLastName.Location = new System.Drawing.Point(154, 62);
            this.PersonLastName.Name = "PersonLastName";
            this.PersonLastName.Size = new System.Drawing.Size(312, 20);
            this.PersonLastName.TabIndex = 3;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(37, 65);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(49, 13);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Apellidos";
            // 
            // Birthday
            // 
            this.Birthday.Enabled = false;
            this.Birthday.Location = new System.Drawing.Point(154, 88);
            this.Birthday.Name = "Birthday";
            this.Birthday.Size = new System.Drawing.Size(312, 20);
            this.Birthday.TabIndex = 5;
            this.Birthday.TextChanged += new System.EventHandler(this.Birthday_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha de Nacimiento";
            // 
            // BirthPlace
            // 
            this.BirthPlace.Location = new System.Drawing.Point(154, 294);
            this.BirthPlace.Name = "BirthPlace";
            this.BirthPlace.Size = new System.Drawing.Size(312, 20);
            this.BirthPlace.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 297);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Lugar de Nacimiento";
            // 
            // Height
            // 
            this.Height.Location = new System.Drawing.Point(154, 320);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(312, 20);
            this.Height.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Estatura (m)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Weight
            // 
            this.Weight.Location = new System.Drawing.Point(154, 346);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(312, 20);
            this.Weight.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 349);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Peso (lb)";
            // 
            // HairColor
            // 
            this.HairColor.Location = new System.Drawing.Point(154, 500);
            this.HairColor.Name = "HairColor";
            this.HairColor.Size = new System.Drawing.Size(312, 20);
            this.HairColor.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 503);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Color de Cabello";
            // 
            // EyeColor
            // 
            this.EyeColor.Location = new System.Drawing.Point(154, 526);
            this.EyeColor.Name = "EyeColor";
            this.EyeColor.Size = new System.Drawing.Size(312, 20);
            this.EyeColor.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 529);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Color de Ojos";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DPI
            // 
            this.DPI.Location = new System.Drawing.Point(154, 552);
            this.DPI.Name = "DPI";
            this.DPI.Size = new System.Drawing.Size(312, 20);
            this.DPI.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 555);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "DPI";
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(154, 578);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(312, 20);
            this.Address.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 581);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Direccion";
            // 
            // HomePhone
            // 
            this.HomePhone.Location = new System.Drawing.Point(154, 604);
            this.HomePhone.Name = "HomePhone";
            this.HomePhone.Size = new System.Drawing.Size(312, 20);
            this.HomePhone.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(37, 607);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Telefono de Casa";
            // 
            // PersonalPhone
            // 
            this.PersonalPhone.Location = new System.Drawing.Point(154, 630);
            this.PersonalPhone.Name = "PersonalPhone";
            this.PersonalPhone.Size = new System.Drawing.Size(312, 20);
            this.PersonalPhone.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 633);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Telefono Personal";
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(154, 656);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(312, 20);
            this.Email.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 659);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Correo Electronico";
            // 
            // PersonPicture
            // 
            this.PersonPicture.Location = new System.Drawing.Point(490, 36);
            this.PersonPicture.Name = "PersonPicture";
            this.PersonPicture.Size = new System.Drawing.Size(271, 275);
            this.PersonPicture.TabIndex = 26;
            this.PersonPicture.TabStop = false;
            // 
            // MaleCheck
            // 
            this.MaleCheck.AutoSize = true;
            this.MaleCheck.Location = new System.Drawing.Point(211, 469);
            this.MaleCheck.Name = "MaleCheck";
            this.MaleCheck.Size = new System.Drawing.Size(74, 17);
            this.MaleCheck.TabIndex = 27;
            this.MaleCheck.Text = "Masculino";
            this.MaleCheck.UseVisualStyleBackColor = true;
            // 
            // FemaleCheck
            // 
            this.FemaleCheck.AutoSize = true;
            this.FemaleCheck.Location = new System.Drawing.Point(313, 469);
            this.FemaleCheck.Name = "FemaleCheck";
            this.FemaleCheck.Size = new System.Drawing.Size(72, 17);
            this.FemaleCheck.TabIndex = 28;
            this.FemaleCheck.Text = "Femenino";
            this.FemaleCheck.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(37, 469);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Sexo";
            // 
            // TakePicture
            // 
            this.TakePicture.Location = new System.Drawing.Point(607, 323);
            this.TakePicture.Name = "TakePicture";
            this.TakePicture.Size = new System.Drawing.Size(154, 23);
            this.TakePicture.TabIndex = 30;
            this.TakePicture.Text = "Cargar Foto";
            this.TakePicture.UseVisualStyleBackColor = true;
            this.TakePicture.Click += new System.EventHandler(this.TakePicture_Click);
            // 
            // UploadDPI
            // 
            this.UploadDPI.Location = new System.Drawing.Point(550, 470);
            this.UploadDPI.Name = "UploadDPI";
            this.UploadDPI.Size = new System.Drawing.Size(154, 23);
            this.UploadDPI.TabIndex = 31;
            this.UploadDPI.Text = "Subir DPI";
            this.UploadDPI.UseVisualStyleBackColor = true;
            this.UploadDPI.Click += new System.EventHandler(this.UploadDPI_Click);
            // 
            // UploadMedic
            // 
            this.UploadMedic.Location = new System.Drawing.Point(550, 502);
            this.UploadMedic.Name = "UploadMedic";
            this.UploadMedic.Size = new System.Drawing.Size(154, 23);
            this.UploadMedic.TabIndex = 32;
            this.UploadMedic.Text = "Subir Certificado Medico";
            this.UploadMedic.UseVisualStyleBackColor = true;
            this.UploadMedic.Click += new System.EventHandler(this.UploadMedic_Click);
            // 
            // error
            // 
            this.error.AutoSize = true;
            this.error.Location = new System.Drawing.Point(487, 446);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(0, 13);
            this.error.TabIndex = 34;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.monthCalendar1.Location = new System.Drawing.Point(202, 120);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 35;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // GenerateNotebook
            // 
            this.GenerateNotebook.Location = new System.Drawing.Point(809, 357);
            this.GenerateNotebook.Name = "GenerateNotebook";
            this.GenerateNotebook.Size = new System.Drawing.Size(227, 23);
            this.GenerateNotebook.TabIndex = 36;
            this.GenerateNotebook.Text = "Generar Libreta";
            this.GenerateNotebook.UseVisualStyleBackColor = true;
            this.GenerateNotebook.Click += new System.EventHandler(this.button1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(771, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Seleccionar Curso:";
            // 
            // monthCalendar2
            // 
            this.monthCalendar2.Location = new System.Drawing.Point(809, 104);
            this.monthCalendar2.Name = "monthCalendar2";
            this.monthCalendar2.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(864, 85);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(107, 13);
            this.label16.TabIndex = 41;
            this.label16.Text = "Fecha de Expedicion";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1126, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "Fecha de Expiracion";
            // 
            // monthCalendar3
            // 
            this.monthCalendar3.Location = new System.Drawing.Point(1071, 104);
            this.monthCalendar3.Name = "monthCalendar3";
            this.monthCalendar3.TabIndex = 42;
            // 
            // GenerateCertificate
            // 
            this.GenerateCertificate.Location = new System.Drawing.Point(809, 298);
            this.GenerateCertificate.Name = "GenerateCertificate";
            this.GenerateCertificate.Size = new System.Drawing.Size(227, 23);
            this.GenerateCertificate.TabIndex = 44;
            this.GenerateCertificate.Text = "Generar Certificado de Curso";
            this.GenerateCertificate.UseVisualStyleBackColor = true;
            this.GenerateCertificate.Click += new System.EventHandler(this.GenerateCertificate_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(236, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(193, 20);
            this.label18.TabIndex = 46;
            this.label18.Text = "Registro de Gente de Mar";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(863, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(311, 20);
            this.label19.TabIndex = 47;
            this.label19.Text = "Generacion de Documentos Gente de Mar";
            // 
            // UploadDiplomaCourse
            // 
            this.UploadDiplomaCourse.Enabled = false;
            this.UploadDiplomaCourse.Location = new System.Drawing.Point(550, 531);
            this.UploadDiplomaCourse.Name = "UploadDiplomaCourse";
            this.UploadDiplomaCourse.Size = new System.Drawing.Size(154, 23);
            this.UploadDiplomaCourse.TabIndex = 48;
            this.UploadDiplomaCourse.Text = "Subir Diplomas de escuela naval";
            this.UploadDiplomaCourse.UseVisualStyleBackColor = true;
            this.UploadDiplomaCourse.Click += new System.EventHandler(this.UploadDiplomaCurse_Click);
            // 
            // GenerateMedics
            // 
            this.GenerateMedics.Location = new System.Drawing.Point(809, 328);
            this.GenerateMedics.Name = "GenerateMedics";
            this.GenerateMedics.Size = new System.Drawing.Size(227, 23);
            this.GenerateMedics.TabIndex = 49;
            this.GenerateMedics.Text = "Generar Certificado Medico";
            this.GenerateMedics.UseVisualStyleBackColor = true;
            this.GenerateMedics.Click += new System.EventHandler(this.GenerateMedics_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1052, 303);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 13);
            this.label20.TabIndex = 50;
            this.label20.Text = "Correlativo de Curso";
            // 
            // CourseCorrelative
            // 
            this.CourseCorrelative.Location = new System.Drawing.Point(1231, 300);
            this.CourseCorrelative.Name = "CourseCorrelative";
            this.CourseCorrelative.Size = new System.Drawing.Size(108, 20);
            this.CourseCorrelative.TabIndex = 51;
            // 
            // CorrelativeMedics
            // 
            this.CorrelativeMedics.Location = new System.Drawing.Point(1231, 331);
            this.CorrelativeMedics.Name = "CorrelativeMedics";
            this.CorrelativeMedics.Size = new System.Drawing.Size(108, 20);
            this.CorrelativeMedics.TabIndex = 53;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1052, 334);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(163, 13);
            this.label21.TabIndex = 52;
            this.label21.Text = "Correlativo de Certificado Medico";
            // 
            // CorrelativeNotebook
            // 
            this.CorrelativeNotebook.Location = new System.Drawing.Point(1231, 360);
            this.CorrelativeNotebook.Name = "CorrelativeNotebook";
            this.CorrelativeNotebook.Size = new System.Drawing.Size(108, 20);
            this.CorrelativeNotebook.TabIndex = 55;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1052, 363);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 54;
            this.label22.Text = "Correlativo de Libreta";
            // 
            // NameOfCourse
            // 
            this.NameOfCourse.FormattingEnabled = true;
            this.NameOfCourse.Items.AddRange(new object[] {
            "PRIMEROS AUXILIOS BÁSICOS",
            "PREVENCIÓN Y LUCHA CONTRA INCENDIOS",
            "SEGURIDAD PERSONAL Y RESPONSABILIDADES SOCIALES",
            "TÉCNICAS DE SUPERVIVENCIA PERSONAL",
            "CONTROL DE MULTITUDES",
            "SUFICIENCIA EN LA FORMACIÓN SOBRE GESTIÓN DE EMERGENCIAS Y COMPORTAMIENTO HUMANO," +
                " INCLUIDAS LA SEGURIDAD DE LOS PASAJEROS, LA CARGA E INTEGRIDAD DEL CASCO",
            "FORMACIÓN DE SENSIBILIZACIÓN SOBRE SEGURIDAD PARA TODA LA GENTE DE MAR",
            "SUFICIENCIA EN EL MANEJO DE EMBARCACIONES DE SUPERVIVENCIA Y BOTES DE RESCATE QUE" +
                " NO SEAN BOTES DE RESCATE RÁPIDO "});
            this.NameOfCourse.Location = new System.Drawing.Point(873, 52);
            this.NameOfCourse.Name = "NameOfCourse";
            this.NameOfCourse.Size = new System.Drawing.Size(260, 21);
            this.NameOfCourse.TabIndex = 57;
            // 
            // NewPerson
            // 
            this.NewPerson.Location = new System.Drawing.Point(550, 592);
            this.NewPerson.Name = "NewPerson";
            this.NewPerson.Size = new System.Drawing.Size(154, 23);
            this.NewPerson.TabIndex = 33;
            this.NewPerson.Text = "Ingresar Persona";
            this.NewPerson.UseVisualStyleBackColor = true;
            this.NewPerson.Click += new System.EventHandler(this.NewPerson_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1185, 592);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "Ingresar Nueva Persona";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Menu
            // 
            this.Menu.Location = new System.Drawing.Point(1071, 592);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(105, 23);
            this.Menu.TabIndex = 62;
            this.Menu.Text = "Regresar a menu";
            this.Menu.UseVisualStyleBackColor = true;
            this.Menu.Click += new System.EventHandler(this.Menu_Click);
            // 
            // PersonNationality
            // 
            this.PersonNationality.Location = new System.Drawing.Point(154, 375);
            this.PersonNationality.Name = "PersonNationality";
            this.PersonNationality.Size = new System.Drawing.Size(312, 20);
            this.PersonNationality.TabIndex = 64;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(37, 377);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 13);
            this.label24.TabIndex = 63;
            this.label24.Text = "Nacionalidad";
            // 
            // PersonCountry
            // 
            this.PersonCountry.Location = new System.Drawing.Point(154, 401);
            this.PersonCountry.Name = "PersonCountry";
            this.PersonCountry.Size = new System.Drawing.Size(312, 20);
            this.PersonCountry.TabIndex = 66;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(37, 404);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 13);
            this.label25.TabIndex = 65;
            this.label25.Text = "Pais de Origen";
            // 
            // PersonState
            // 
            this.PersonState.Location = new System.Drawing.Point(154, 427);
            this.PersonState.Name = "PersonState";
            this.PersonState.Size = new System.Drawing.Size(312, 20);
            this.PersonState.TabIndex = 68;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(38, 433);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 13);
            this.label26.TabIndex = 67;
            this.label26.Text = "Departamento";
            // 
            // CertificateRequest
            // 
            this.CertificateRequest.Enabled = false;
            this.CertificateRequest.Location = new System.Drawing.Point(550, 560);
            this.CertificateRequest.Name = "CertificateRequest";
            this.CertificateRequest.Size = new System.Drawing.Size(154, 23);
            this.CertificateRequest.TabIndex = 69;
            this.CertificateRequest.Text = "Subir Solicitud de Certificado";
            this.CertificateRequest.UseVisualStyleBackColor = true;
            this.CertificateRequest.Click += new System.EventHandler(this.CertificateRequest_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(543, 439);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(169, 20);
            this.label23.TabIndex = 70;
            this.label23.Text = "Carga de Documentos";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1351, 693);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.CertificateRequest);
            this.Controls.Add(this.PersonState);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.PersonCountry);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.PersonNationality);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Menu);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.NameOfCourse);
            this.Controls.Add(this.CorrelativeNotebook);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.CorrelativeMedics);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.CourseCorrelative);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.GenerateMedics);
            this.Controls.Add(this.UploadDiplomaCourse);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.GenerateCertificate);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.monthCalendar3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.monthCalendar2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.GenerateNotebook);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.error);
            this.Controls.Add(this.NewPerson);
            this.Controls.Add(this.UploadMedic);
            this.Controls.Add(this.UploadDPI);
            this.Controls.Add(this.TakePicture);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.FemaleCheck);
            this.Controls.Add(this.MaleCheck);
            this.Controls.Add(this.PersonPicture);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.PersonalPhone);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.HomePhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DPI);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.EyeColor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.HairColor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Weight);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Height);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BirthPlace);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Birthday);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PersonLastName);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.PersonName);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PersonPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PersonName;
        private System.Windows.Forms.TextBox PersonLastName;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.TextBox Birthday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BirthPlace;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Height;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Weight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox HairColor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox EyeColor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox DPI;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox HomePhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox PersonalPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox PersonPicture;
        private System.Windows.Forms.CheckBox MaleCheck;
        private System.Windows.Forms.CheckBox FemaleCheck;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button TakePicture;
        private System.Windows.Forms.Button UploadDPI;
        private System.Windows.Forms.Button UploadMedic;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button GenerateNotebook;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MonthCalendar monthCalendar2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MonthCalendar monthCalendar3;
        private System.Windows.Forms.Button GenerateCertificate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button UploadDiplomaCourse;
        private System.Windows.Forms.Button GenerateMedics;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox CourseCorrelative;
        private System.Windows.Forms.TextBox CorrelativeMedics;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox CorrelativeNotebook;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox NameOfCourse;
        private System.Windows.Forms.Button NewPerson;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Menu;
        private System.Windows.Forms.TextBox PersonNationality;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox PersonCountry;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox PersonState;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button CertificateRequest;
        private System.Windows.Forms.Label label23;
    }
}