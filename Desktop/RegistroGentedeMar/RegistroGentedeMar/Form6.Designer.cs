﻿namespace RegistroGentedeMar
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.CourseName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.Button();
            this.error = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 20);
            this.label15.TabIndex = 129;
            this.label15.Text = "Creacion de Curso";
            // 
            // CourseName
            // 
            this.CourseName.Enabled = false;
            this.CourseName.Location = new System.Drawing.Point(134, 64);
            this.CourseName.Name = "CourseName";
            this.CourseName.Size = new System.Drawing.Size(275, 20);
            this.CourseName.TabIndex = 131;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 130;
            this.label3.Text = "Nombre del curso:";
            // 
            // menu
            // 
            this.menu.Location = new System.Drawing.Point(437, 130);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(154, 23);
            this.menu.TabIndex = 133;
            this.menu.Text = "Menu";
            this.menu.UseVisualStyleBackColor = true;
            this.menu.Click += new System.EventHandler(this.menu_Click);
            // 
            // error
            // 
            this.error.AutoSize = true;
            this.error.Location = new System.Drawing.Point(25, 105);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(35, 13);
            this.error.TabIndex = 134;
            this.error.Text = "label1";
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 191);
            this.Controls.Add(this.error);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.CourseName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label15);
            this.Name = "Form6";
            this.Text = "Form6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox CourseName;
        private System.Windows.Forms.Label label3;
       // private System.Windows.Forms.Button createCourses;
        private System.Windows.Forms.Button menu;
        private System.Windows.Forms.Label error;
    }
}