﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RegistroGentedeMar
{
    class saveUpload
    {
        public String save() {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            DialogResult dr = saveFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                return saveFileDialog1.FileName;
            }
            return "";
        }

        public String upload(String Variable) {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String path = dialog.FileName;
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), new UTF8Encoding()))
                {
                    reader.Close();
                    Variable = path;
                }
            }
            return Variable;
        }
    }
}
