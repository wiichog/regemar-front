﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace RegistroGentedeMar
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void menu_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form2 menu = new Form2();
            menu.Visible = true;
        }

        private void createCourse_Click(object sender, EventArgs e)
        {
            createCourses(CourseName.Text);
        }

        public String createCourses(String nameOfCourse) {
            try
            {
                encrypt en = new encrypt();
                string MyConnection2 = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
                string Query = "INSERT INTO `cursos`" +
                    "(`nombreCurso`) " +
                    "VALUES('" + nameOfCourse + "');";
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                Console.Write(Query);
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Curso: " + nameOfCourse + " creado exitosamente";
        }
    }
}
