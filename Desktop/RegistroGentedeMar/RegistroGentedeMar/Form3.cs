﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System;


namespace RegistroGentedeMar
{
    public partial class Form3 : Form
    {
        public String DPISource = "";
        public String ImageSource = "";
        public String MedicsSource = "";
        public String DiplomaSource = "";
        public String CertificateRequestSource = "";
        public int curseCounter = 0;
        public Form3()
        {
            InitializeComponent();
        }

        private void NewPerson_Click(object sender, EventArgs e)
        {
            if (CheckInformation() == true) {
                ftpserver ftp = new ftpserver();
                ftp.CreateDirectory(DPI.Text);
                ftp.uploadFile(ImageSource, DPI.Text, "Picture",".jpg");
                ftp.uploadFile(DPISource, DPI.Text, "DPI", ".pdf");
                ftp.uploadFile(MedicsSource, DPI.Text, "Medics", ".pdf");
                ftp.uploadFile(DiplomaSource, DPI.Text, DPI.Text + "Diplomas", ".pdf");
                ftp.uploadFile(CertificateRequestSource, DPI.Text, DPI.Text + "CertificateRequest", ".pdf");
                string Gender = "";
                if (MaleCheck.Checked == true) {
                    Gender = "M";
                }
                else if (FemaleCheck.Checked == true) {
                    Gender = "F";
                }
                error.Text=InsertData(PersonName.Text, PersonLastName.Text, Birthday.Text, PersonNationality.Text,PersonState.Text,PersonCountry.Text,
                    Height.Text, Weight.Text, HairColor.Text, EyeColor.Text, DPI.Text, Address.Text, HomePhone.Text, PersonalPhone.Text,
                    Email.Text, Gender, "dgam.gob.gt/regemaruvg/" + DPI.Text + "/" + DPI.Text + "Picture.jpg",
                    "dgam.gob.gt/regemaruvg/" + DPI.Text + "/" + DPI.Text + "DPI.pdf", "dgam.gob.gt/regemaruvg/" + DPI.Text + "/" + DPI.Text + "Medics.jpg");
                enableComponents(true);
                disableComponents(false);
            }
            else {

            }
        }

        public Boolean CheckInformation()
        {
            if (PersonName.Text == "" || PersonLastName.Text == "" || Birthday.Text == "" || BirthPlace.Text == "" || Height.Text == ""
                || Weight.Text == "" || HairColor.Text == "" || EyeColor.Text == "" || DPI.Text == "" || Address.Text == ""
                || HomePhone.Text == "" || DPISource == "" || ImageSource == "" || MedicsSource == ""  || PersonalPhone.Text == "" || Email.Text == "" || (MaleCheck.Checked == false && FemaleCheck.Checked == false))
            {
                error.Text = "Por favor llenar todos los campos";
                return false;
            }
            return true;
        }

        public String InsertData(String PersonName, String PersonLastName, String PersonBirthday, String PersonNationality, String PersonState, String PersonCountry, String PersonHigh,
            String PersonWeight, String PersonHairColor, String PersonEyeColor, String PersonDPI, String PersonDirection,
            String PersonHomePhone, String PersonPersonalPhone, String PersonMail, String Gender, String PersonPicture, String PersonDPIPicture, String PersonMedicsPicture)
        {
            try
            {
                encrypt en = new encrypt();
                string MyConnection2 = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
                string Query = "INSERT INTO `persona`" +
                    "(`DPI`, `nombre`, `apellido`, `fechaNacimiento`, `estatura`," +
                    " `peso`, `sexo`, `colorCabello`, `colorOjos`, `direccion`, `telefonoCasa`," +
                    " `telefonoCelular`, `correoElectronico`, `fotoPersona`, `fotoDPI`, `fotoCertificadoMedico`, `fotoCertificadoMedico`) " +
                    "VALUES('" + PersonDPI + "','" + PersonName + "','" + PersonLastName + "'" +
                    ",'" + PersonBirthday + "','" + PersonHigh + "','" + PersonWeight + "','" + Gender + "','" + PersonHairColor + "','" + PersonEyeColor + "'" +
                    ",'" + PersonDirection + "','" + PersonHomePhone + "','" + PersonPersonalPhone + "','" + PersonMail + "','" + PersonPicture + "'," +
                    "'" + PersonDPIPicture + "','" + PersonMedicsPicture + "');";
                error.Text = PersonPicture;
                Console.Write(Query);
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                Console.Write(Query);
                MyReader2 = MyCommand2.ExecuteReader();    
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return "Usuario: "+ PersonName + " " + PersonLastName + " creado exitosamente";
        }

        private void fillCurses()
        {
            encrypt en = new encrypt();
            using (MySqlConnection conn = new MySqlConnection(en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=")))
            {
                try
                {
                    string query = "SELECT idCurso,nombreCurso FROM cursos";
                    MySqlDataAdapter da = new MySqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Curses");
                    NameOfCourse.DisplayMember = "nombreCurso";
                    NameOfCourse.ValueMember = "idCurso";
                    NameOfCourse.DataSource = ds.Tables["Curses"];
                    conn.Close();
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show("Error occured!");
                }
            }
        }

        private void Birthday_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            var monthCalendar = sender as MonthCalendar;
            Birthday.Text = monthCalendar1.SelectionStart.ToShortDateString();
        }

        private void TakePicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK) 
            {
                String path = dialog.FileName;
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), new UTF8Encoding()))
                {
                    PersonPicture.ImageLocation = path;
                    PersonPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                    ImageSource = path;
                    reader.Close();
                }
            }
        }

        private void UploadDPI_Click(object sender, EventArgs e)
        {
            saveUpload upload = new saveUpload();
            upload.upload(DPISource);
        }

        private void UploadMedic_Click(object sender, EventArgs e)
        {
            saveUpload upload = new saveUpload();
            upload.upload(MedicsSource);
        }

        private void UploadDiplomaCurse_Click(object sender, EventArgs e)
        {
            saveUpload upload = new saveUpload();
            upload.upload(DiplomaSource);
        }

        
        public String AddCurse(String DPI, String CurseId,String CertificateImage, String DiplomaImage, String ExpeditionDate, String ExpirationDate)
        {
            try
            {
                encrypt en = new encrypt();
                string MyConnection2 = en.Desencriptar("5qqZwIz6qANz9uyqpTWgiAAltr4cJvp/zNUEBsJx6syRMFCaX1lUfXHgHvupHknn9n+9HfP5pH01tZwuR0qE5Qv6Rm4fmljSFd08gpNZmXM=");
                string Query = "INSERT INTO `personaCertificado`(`dpi`, `idCurso`, `imagenCertificado`, `imagenDiploma`, `fechaExpedicion`, `fechaExpiracion`) " +
                    "VALUES('" + DPI + "','" + CurseId + "','" + CertificateImage + "'" +
                    ",'" + DiplomaImage + "','" + ExpeditionDate + "','" + ExpirationDate + "');";

                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                Console.Write(Query);
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return "Curso: " + PersonName + " " + PersonLastName + " creado exitosamente";
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            fillCurses();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form3 nueva = new Form3();
            this.Visible = false;
            nueva.Show();
        }

        public void NewPersona() {
            DPISource = "";
            ImageSource = "";
            MedicsSource = "";
            disableComponents(true);
            enableComponents(false);
            PersonName.Text = "";
            PersonLastName.Text = "";
            Birthday.Text = "";
            BirthPlace.Text = "";
            Height.Text = "";
            Weight.Text = "";
            MaleCheck.Checked = false;
            FemaleCheck.Checked = false;
            HairColor.Text = "";
            EyeColor.Text = "";
            DPI.Text = "";
            Address.Text = "";
            HomePhone.Text = "";
            PersonalPhone.Text = "";
            Email.Text = "";
            monthCalendar1.SetDate(DateTime.Today);
            error.Text = "";
            monthCalendar2.SetDate(DateTime.Today);
            monthCalendar3.SetDate(DateTime.Today);
            CorrelativeMedics.Text = "";
            CorrelativeNotebook.Text = "";
            CourseCorrelative.Text = "";
        }

        public void enableComponents(Boolean value)
        {
            NameOfCourse.Enabled = value;
            UploadDiplomaCourse.Enabled = value;
            monthCalendar2.Enabled = value;
            monthCalendar3.Enabled = value;
            GenerateCertificate.Enabled = value;
            GenerateMedics.Enabled = value;
            GenerateNotebook.Enabled = value;
            CorrelativeMedics.Enabled = value;
            CorrelativeNotebook.Enabled = value;
            CourseCorrelative.Enabled = value;
        }

        public void disableComponents(Boolean value)
        {
            PersonName.Enabled = value;
            PersonLastName.Enabled = value;
            monthCalendar1.Enabled = value;
            BirthPlace.Enabled = value;
            Height.Enabled = value;
            Weight.Enabled = value;
            EyeColor.Enabled = value;
            HairColor.Enabled = value;
            DPI.Enabled = value;
            Address.Enabled = value;
            HomePhone.Enabled = value;
            PersonalPhone.Enabled = value;
            Email.Enabled = value;
            MaleCheck.Enabled = value;
            FemaleCheck.Enabled = value;
            TakePicture.Enabled = value;
            UploadDPI.Enabled = value;
            UploadMedic.Enabled = value;
        }


        private void Menu_Click(object sender, EventArgs e)
        {
            Form2 menu = new Form2();
            menu.Visible = true;
            this.Visible = false;
        }

        private void CertificateRequest_Click(object sender, EventArgs e)
        {
            saveUpload upload = new saveUpload();
            upload.upload(CertificateRequestSource);
        }

        private void GenerateMedics_Click(object sender, EventArgs e)
        {
            //generateDocument doc = new generateDocument();
           // doc.medical();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  generateDocument doc = new generateDocument();
           // doc.notebook();
        }

        private void GenerateCertificate_Click(object sender, EventArgs e)
        {
         //   generateDocument doc = new generateDocument();
          //  doc.course();
        }

    }
}
